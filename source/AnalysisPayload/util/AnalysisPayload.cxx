// stdlib functionality
#include <iostream>
#include <vector>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "JetSelectionHelper/JetSelectionHelper.h"

// jet calibration
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"

int main(int argc, char** argv) {

  // initialize the xAOD EDM
  xAOD::Init();

  // declare the tool
  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");
  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  );
	JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
	JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              );
	JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       );
	JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            ); 
  JetCalibrationTool_handle.retrieve();


  // initialize the selector
  JetSelectionHelper jet_selector;

  // open the input file
  TString inputFilePath;
  if(argc >= 2) inputFilePath = argv[1];

  // get the output file path
  TString outputFilePath;
  if(argc >= 3) outputFilePath = argv[2];

  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  // declare the histograms
  TH1D* nJet = new TH1D("njets","",20,0,20);
  TH1D* nJetGood = new TH1D("njetsGood","",20,0,20);
  TH1D* nJetB = new TH1D("njetsB","",20,0,20);
  TH1D* mjj = new TH1D("mjj","",20,0,500);
  TH1D* mjjGood = new TH1D("mjjGood","",20,0,500);
  TH1D* mjjbJets = new TH1D("mjjB","",20,0,500);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  Long64_t numEntries(-1);
  if(argc >= 4) numEntries  = std::atoi(argv[3]);

  if(numEntries == -1) numEntries = event.getEntries();
  
  std::cout << "Processing " << numEntries << " events" << std::endl;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    float mc_evt_weight_nom = ei->mcEventWeights().at(0);    // Use the 0th entry for "nominal" event weight

    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << ". MC event weight: " << mc_evt_weight_nom << std::endl;

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    std::vector<xAOD::Jet> allJets;
    std::vector<xAOD::Jet> jetsPassSel;
    std::vector<xAOD::Jet> bJets;

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
			// calibrate the jet
			xAOD::Jet *calibratedjet;
			JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);

      allJets.push_back(*calibratedjet);
      if(jet_selector.isJetGood(calibratedjet)){
        jetsPassSel.push_back(*calibratedjet);
      }
      if(jet_selector.isJetBFlavor(calibratedjet)){
        bJets.push_back(*calibratedjet);
      }

      delete calibratedjet;
    }

    nJet->Fill( allJets.size() );
    nJetGood->Fill( jetsPassSel.size() );
    nJetB->Fill( bJets.size() );

    if( allJets.size() > 1) {
      mjj->Fill( ( allJets.at(0).p4() + allJets.at(1).p4() ).M()/1000 );
    }
    if( jetsPassSel.size() > 1) {
      mjjGood->Fill( ( jetsPassSel.at(0).p4() + jetsPassSel.at(1).p4() ).M()/1000 );
    }
    if( bJets.size() > 1) {
      mjjbJets->Fill( ( bJets.at(0).p4() + bJets.at(1).p4() ).M()/1000 );
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  TFile *fout = new TFile(outputFilePath,"RECREATE");

  nJet->Write();
  nJetGood->Write();
  nJetB->Write();
  mjj->Write();
  mjjGood->Write();
  mjjbJets->Write();

  fout->Close();

  // exit from the main function cleanly
  return 0;
}
